﻿using System;
using System.Collections.Generic;

namespace RPPOON___LV_4_6
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> emails = new List<string>();
            emails.Add("divanovic@etfos.hr");
            emails.Add("divanovic@etfos.ru");
            emails.Add("divanovicetfos.ca");
            emails.Add("divanovic@etfos.com");

            IEmailValidator emailValidator = new EmailValidator();

            foreach (string email in emails)
            {
                if (emailValidator.IsValidAddress(email))
                {
                    Console.WriteLine("Email je ispravan.");
                }
                else
                {
                    Console.WriteLine("Email nije ispravan.");
                }
            }


        }
    }
}
