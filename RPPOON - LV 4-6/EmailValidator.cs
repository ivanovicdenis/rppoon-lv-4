﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON___LV_4_6
{
    class EmailValidator : IEmailValidator
    {
        public bool IsValidAddress(String candidate)
        {
            if (String.IsNullOrEmpty(candidate))
            {
                return false;
            }
            return IsEndingWithProperDomain(candidate) && ContainsAt(candidate);
        }
        private bool IsEndingWithProperDomain(String candidate)
        {
            bool endsWithCom = false, endsWithHr=false;

            if (candidate.EndsWith(".com")) endsWithCom = true;
            if (candidate.EndsWith(".hr")) endsWithHr = true;

            return (endsWithCom || endsWithHr);
        }
        private bool ContainsAt(string candidate)
        {
            if (candidate.Contains("@"))
                return true;
            else return false;
        }
    }
}
