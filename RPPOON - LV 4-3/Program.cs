﻿using System;
using System.Collections.Generic;

namespace RPPOON___LV_4_3
{
    class Program
    {
        static void Main(string[] args)
        {
            RentingConsolePrinter printer = new RentingConsolePrinter();
            IRentable book = new Book("Pjesma vatre i leda");
            IRentable video = new Video("Game of Thrones");
            List<IRentable> items = new List<IRentable>();
            items.Add(book);
            items.Add(video);

            printer.DisplayItems(items);
            Console.Write("Total price: ");
            printer.PrintTotalPrice(items);
        }
    }
}
