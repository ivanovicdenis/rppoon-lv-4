﻿using System;
using System.Collections.Generic;

namespace RPPOON___LV_4_5
{
    class Program
    {
        static void Main(string[] args)
        {
            RentingConsolePrinter printer = new RentingConsolePrinter();
            IRentable book = new Book("Pjesma vatre i leda");
            IRentable video = new Video("Game of Thrones");
            List<IRentable> items = new List<IRentable>();
            items.Add(book);
            items.Add(video);

            List<IRentable> flashSale = new List<IRentable>();
            foreach (IRentable item in items)
            {
                flashSale.Add(new DiscountedItem(item, 28.08));
            }

            printer.DisplayItems(flashSale);
            Console.Write("Total price: ");
            printer.PrintTotalPrice(flashSale);
        }
    }
}
