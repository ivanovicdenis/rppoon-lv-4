﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON___LV_4_5
{
    class DiscountedItem : RentableDecorator
    {
        private readonly double DiscountProcent;
        public DiscountedItem(IRentable rentable, double discountProcent) : base(rentable) 
        {
            this.DiscountProcent = discountProcent;
        }
        public override double CalculatePrice()
        {
            return base.CalculatePrice() - base.CalculatePrice()*this.DiscountProcent/100;
        }
        public override String Description
        {
            get
            {
                return base.Description + " now at ["+DiscountProcent.ToString()+"]% off!";
            }
        }
    }
}
