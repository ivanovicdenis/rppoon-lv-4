﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON___LV_4_5
{
    class Video : IRentable
    {
        private readonly double BaseVideoPrice = 9.99;
        public String Name { get; private set; }
        public Video(String name) { this.Name = name; }
        public string Description { get { return "Video: " + this.Name; } }
        public double CalculatePrice() { return BaseVideoPrice; }
    }
}
