﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON___LV_4_5
{
    interface IRentable
    {
        String Description { get; }
        double CalculatePrice();
    }
}
