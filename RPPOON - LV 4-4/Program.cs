﻿using System;
using System.Collections.Generic;

namespace RPPOON___LV_4_4
{
    class Program
    {
        static void Main(string[] args)
        {

            RentingConsolePrinter printer = new RentingConsolePrinter();
            IRentable book = new Book("Pjesma vatre i leda");
            IRentable video = new Video("Game of Thrones");
            List<IRentable> items = new List<IRentable>();

            items.Add(book);
            items.Add(video);

            IRentable hotBook = new HotItem(book);
            IRentable hotVideo = new HotItem(video);

            items.Add(hotBook);
            items.Add(hotVideo);

            printer.DisplayItems(items);
            Console.Write("Total price: ");
            printer.PrintTotalPrice(items);
        }
    }
}
