﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON___LV_4_4
{
    class Book : IRentable
    {
        private readonly double BaseVideoPrice = 3.99;
        public String title { get; private set; }
        public Book(String title) { this.title = title; }
        public string Description { get { return "Book: " + this.title; } }
        public double CalculatePrice() { return BaseVideoPrice; }
    }
}
