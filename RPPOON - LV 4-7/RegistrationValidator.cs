﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON___LV_4_7
{
    class RegistrationValidator : IRegistrationValidator
    {
        private IEmailValidator emailValidator;
        private IPasswordValidatorService passwordValidator; 

        public RegistrationValidator()
        {
            emailValidator = new EmailValidator();
            passwordValidator = new PasswordValidator(6);
        }
        public bool IsUserEntryValid(UserEntry entry)
        {
            return (emailValidator.IsValidAddress(entry.Email) && 
                passwordValidator.IsValidPassword(entry.Password));
        }
    }
}
