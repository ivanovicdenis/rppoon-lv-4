﻿using System;

namespace RPPOON___LV_4_7
{
    class Program
    {
        static void Main(string[] args)
        {
            IRegistrationValidator registrationValidator = new RegistrationValidator();
            UserEntry userEntry;
            do
            {
                userEntry = UserEntry.ReadUserFromConsole();

                if(!registrationValidator.IsUserEntryValid(userEntry)) 
                    Console.WriteLine("Pogresan unos pokusajte ponovno!");
            } while (!registrationValidator.IsUserEntryValid(userEntry));
        }
    }
}