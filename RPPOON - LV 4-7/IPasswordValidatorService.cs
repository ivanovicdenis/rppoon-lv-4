﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON___LV_4_7
{
    interface IPasswordValidatorService
    {
        bool IsValidPassword(String candidate);
    }
}
