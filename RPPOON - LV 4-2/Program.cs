﻿using System;

namespace RPPOON___LV_4_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset dataset = new Dataset("data.csv");
            Analyzer3rdParty analyzer3RdParty = new Analyzer3rdParty();
            Adapter adapter = new Adapter(analyzer3RdParty);
            double[] averagePerRow = adapter.CalculateAveragePerRow(dataset);
            double[] averagePerColumn = adapter.CalculateAveragePerColumn(dataset);

            for (int i = 0; i < averagePerColumn.Length; i++)
            {
                Console.WriteLine(averagePerColumn[i].ToString());
            }
            Console.WriteLine();
            for (int i = 0; i < averagePerRow.Length; i++)
            {
                Console.WriteLine(averagePerRow[i].ToString());
            }
        }
    }
}
