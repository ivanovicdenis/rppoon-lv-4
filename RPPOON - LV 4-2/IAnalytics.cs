﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON___LV_4_2
{
    interface IAnalytics
    {
        double[] CalculateAveragePerColumn(Dataset dataset);
        double[] CalculateAveragePerRow(Dataset dataset);
    }
}
